let ipcRenderer = require('electron').ipcRenderer;
let proccesing = false;
function submitForm(){
    if (proccesing) {
        return;
    }
    proccesing = true;
    document.getElementById('result').classList.add('hide');
    document.getElementById('error').classList.add('hide');
    document.getElementById('loading').classList.remove('hide');
    var formData = {};

    formData.type = document.getElementById('scraping-type').value;
    formData.id = document.getElementById('id').value;
    formData.number = document.getElementById('number').value;
    formData.proxy = document.getElementById('proxy').value;
    formData.timeout = document.getElementById('timeout').value;
    formData.filepath = document.getElementById('filepath').value;
    ipcRenderer.send('submitForm', formData);

}


ipcRenderer.on('formSubmissionResults', function(event, args) {
    proccesing=false;
    let results = args;
    document.getElementById('loading').classList.add('hide');
    if (results.status) {
        alert("completed!");
        document.getElementById('result').innerHTML = "Done";
        document.getElementById('result').classList.remove('hide');
    } else{
        alert("failed!");
        document.getElementById('error').innerHTML = results.error;
        document.getElementById('error').classList.remove('hide');
    }
});