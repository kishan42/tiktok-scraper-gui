const fs = require('fs');
const path = require('path')
const { app, BrowserWindow, ipcMain } = require('electron')
const TiktokScraper = require('./TiktokScraper');


const Terminal = require('xterm').Terminal;
global.terminal = new Terminal();


function createWindow () {
  // Create the browser window.
  const win = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true
    }
  })

  win.maximize();

  // win.webContents.openDevTools();

  // and load the index.html of the app.
  win.loadFile('./assets/index.html')

}

ipcMain.on('submitForm', function(event, data) {
  // Access form data here
  //console.log(data);

  scraper = new TiktokScraper(data);

  scraper.download(event.sender);
  
  //event.sender.send('formSubmissionResults', {result:true});

});
const homedir = require('os').homedir();
var downloadsPath = path.resolve(homedir+'/tiktokscraper');
console.log('====================================');
console.log(downloadsPath);
console.log('====================================');
if (!fs.existsSync(downloadsPath)){
  fs.mkdir(downloadsPath, () => {
    app.whenReady().then(createWindow);
  });
}else{
  app.whenReady().then(createWindow);
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})