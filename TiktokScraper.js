const ts = require('tiktok-scraper');
const { fork } = require('child_process');
const path = require('path')
//const Cli = require('')

class TiktokScraper {

    cmdPath = __dirname + "/node_modules/.bin/tiktok-scraper";

    options = {
        download: true,
        //cli:process.cli
    };
    method = '';
    id = "";

    constructor(options){
        this.setOptions(options);
        options.cli = global.terminal;
    }

    run(sender){
        let cmd = this.buildCmd();
        console.log(cmd);
    }
    setOptions(config) {
        this.method = config.type;

        if (config.type !== "trend") {
            this.id = config.id
        }

        if (config.number) {
            this.options.number = config.number
        }

        if (config.proxy) {
            this.options.proxy = config.proxy
        }

        if (config.timeout) {
            this.options.timeout = config.timeout
        }
        
        const homedir = require('os').homedir();
        var downloadsPath = path.resolve(homedir+'/tiktokscraper');

        this.options.filepath = config.filepath ? config.filepath : downloadsPath;

    }

    async download(sender){
        try {
            let handle = await ts[this.method](this.id,this.options);
            sender.send('formSubmissionResults', {status:true});
        } catch (error) {
            sender.send('formSubmissionResults', {result:false,error:error});
        }
    }


    buildCmd(){

        let _options = "";

        for (const key in this.options) {
            if (this.options.hasOwnProperty(key)) {
                const option = this.options[key];
                _options += " --" + key + " " + option;
            }
        }
        var _call = this.method;
        
        if(this.id){
            _call = _call + " " + this.id;
        }

        return this.cmdPath + " " + _call + " -d " + _options;
    }
}

module.exports = TiktokScraper;